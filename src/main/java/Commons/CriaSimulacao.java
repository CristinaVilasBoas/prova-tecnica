package Commons;

import static io.restassured.RestAssured.given;

import java.math.BigDecimal;

import org.apache.http.HttpStatus;

import Base.BaseSimulacao;
import Model.Simulacao;
import Util.GeradorDeDados;
import io.restassured.http.ContentType;

public class CriaSimulacao extends BaseSimulacao {
	GeradorDeDados geradorDeDados = new GeradorDeDados();

	public CriaSimulacao() {
		BaseSimulacao.initConfig();
	}

	public Simulacao preencherSimulacaoValida() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	
	public Simulacao preencherSimulacaoComCpfInvalido() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio()).cpf("034")
				.email("cristina.silva@email.com").valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao preencherSimulacaoComEmailInvalido() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio()).cpf("034")
				.email("cristina.silva").valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao preencherSimulacaoComValorMenorDe1000() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("999.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComValorMaiorDe41000() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("41000.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComValorMaiorDe1000() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("1100.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComValorMenorDe40000() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("39000.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComValorIgualDe1000() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("1000.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComValorIgualDe40000() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("40000.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComParcelaIgualA2() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("40000.00")).parcelas(2).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComParcelaMaior2() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("40000.00")).parcelas(10).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComParcelaIgual48() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("40000.00")).parcelas(48).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComParcelaMenorA2() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("40000.00")).parcelas(1).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoComParcelaMaiorA48() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email(geradorDeDados.geraEmailValido())
				.valor(new BigDecimal("40000.00")).parcelas(49).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoSemNome() {
		Simulacao simulacao = Simulacao.builder().cpf(geradorDeDados.geraCpfSemRepetir())
				.email("cristina.silva@email.com").valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoSemCpf() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.email("cristina.silva@email.com").valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoSemEmail() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).valor(new BigDecimal("30000.00")).parcelas(5).seguro(true)
				.build();
		return simulacao;
	}

	public Simulacao criarSimulacaoSemValor() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com").parcelas(5).seguro(true)
				.build();
		return simulacao;
	}

	public Simulacao criarSimulacaoSemParcela() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).seguro(true).build();
		return simulacao;
	}

	public Simulacao criarSimulacaoSemSeguro() {
		Simulacao simulacao = Simulacao.builder().nome(geradorDeDados.geraNomeAleatorio())
				.cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).build();
		return simulacao;
	}

	public void cadastrarSimulacaoSemOnome() {
		given().contentType(ContentType.JSON).body(criarSimulacaoSemNome()).when().post("/v1/simulacoes")
		.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}
	
	public void cadastrarSimulacaoSemOcpf() {
		given().contentType(ContentType.JSON).body(criarSimulacaoSemCpf()).when().post("/v1/simulacoes")
		.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}
	
	public void cadastrarSimulacaoSemOemail() {
		given().contentType(ContentType.JSON).body(criarSimulacaoSemEmail()).when().post("/v1/simulacoes")
		.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}
	
	public void cadastrarSimulacaoSemOvalor() {
		given().contentType(ContentType.JSON).body(criarSimulacaoSemValor()).when().post("/v1/simulacoes")
		.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}
	
	public void cadastrarSimulacaoSemOParcela() {
		given().contentType(ContentType.JSON).body(criarSimulacaoSemParcela()).when().post("/v1/simulacoes")
		.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}
	
	public void cadastrarSimulacaoSemOSeguro() {
		given().contentType(ContentType.JSON).body(criarSimulacaoSemSeguro()).when().post("/v1/simulacoes")
		.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}
	
	public void cadastrarSimulacaoComParcelaMenorA2() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComParcelaIgualA2()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	public void cadastrarSimulacaoComParcelaMaior48() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComParcelaMaiorA48()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	public void cadastrarSimulacaoComParcelaIgualA2() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComParcelaIgualA2()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_CREATED);
	}

	public void cadastrarSimulacaoComParcelaMaiorA2() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComParcelaMaior2()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_CREATED);
	}

	public void cadastrarSimulacaoComParcelaIgual48() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComParcelaIgual48()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_CREATED);
	}

	public void cadastrarSimulacaoComSucesso() {
		given().contentType(ContentType.JSON).body(preencherSimulacaoValida()).when().post("/v1/simulacoes").then()
				.statusCode(HttpStatus.SC_CREATED);
	}

	public void cadastrarSimulacaoComCpfInvalido() {
		given().contentType(ContentType.JSON).body(preencherSimulacaoComCpfInvalido()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	public void cadastrarSimulacaoComEmailInvalido() {
		given().contentType(ContentType.JSON).body(preencherSimulacaoComEmailInvalido()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	public void cadastrarSimulacaoComValorMenorDe1000() {
		given().contentType(ContentType.JSON).body(preencherSimulacaoComValorMenorDe1000()).when()
				.post("/v1/simulacoes").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	public void cadastrarSimulacaoComValorMaiorDe1000() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComValorMaiorDe1000()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_CREATED);
	}

	public void cadastrarSimulacaoComValorMaiorDe41000() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComValorMaiorDe41000()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	public void cadastraSimulacaoComValorMenorDe40000() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComValorMenorDe40000()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_CREATED);
	}

	public void cadastrarSimulacaoComValorIgualDe1000() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComValorIgualDe1000()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_CREATED);
	}

	public void cadastrarSimulacaoComValorIgualDe40000() {
		given().contentType(ContentType.JSON).body(criarSimulacaoComValorIgualDe40000()).when().post("/v1/simulacoes")
				.then().statusCode(HttpStatus.SC_CREATED);
	}
}

package Commons;

import static io.restassured.RestAssured.given;

import org.apache.http.HttpStatus;

import Base.BaseSimulacao;
import io.restassured.http.ContentType;

public class ConsultarSimulacoes extends BaseSimulacao{

	public ConsultarSimulacoes() {
		 BaseSimulacao.initConfig();
	}
	
	public void consultarSimulacoes() {
		given().
			contentType(ContentType.JSON).
		when().
			get("/v1/simulacoes").
		then().
			statusCode(HttpStatus.SC_OK);
	}
}

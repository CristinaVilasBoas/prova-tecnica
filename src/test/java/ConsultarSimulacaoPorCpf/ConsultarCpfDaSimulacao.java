package ConsultarSimulacaoPorCpf;

import org.junit.Test;
import Commons.ConsultarPorCpf;
import Util.GeradorDeDados;

public class ConsultarCpfDaSimulacao {
	GeradorDeDados geradorDeDados = new GeradorDeDados();
	ConsultarPorCpf consultarPorCpf = new ConsultarPorCpf();

	@Test
	public void consultarPorCpfInexistente() {
		consultarPorCpf.consultaPorCpfInexistente(geradorDeDados.geraCpfSemRepetir());
	}

	@Test
	public void consultarPorCpfExistente() {
		consultarPorCpf.consultarPorCpfExistente();
	}
}

package Commons;

import static io.restassured.RestAssured.given;

import java.math.BigDecimal;

import org.apache.http.HttpStatus;

import Base.BaseSimulacao;
import Model.Simulacao;
import Util.GeradorDeDados;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ResponseBodyExtractionOptions;

public class AlterarSimulacao extends BaseSimulacao {
	
	GeradorDeDados geradorDeDados = new GeradorDeDados();

	public AlterarSimulacao() {
		BaseSimulacao.initConfig();
	}
	
	public void alterarSimulacaoComSucesso(){
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_OK);
	}
	
	public void alterarSimulacaoInvalida() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		String idExtraido = jsonPath.getString("id");
		
		given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_NOT_FOUND);

	}
	
	public void alterarSimulacaoComEmailInvalido() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		String idExtraido = jsonPath.getString("id");
		
		given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_NOT_FOUND);

	}
	
	public void alterarSimulacaoComCpfInvalido() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		String idExtraido = jsonPath.getString("id");
		
		given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf("45").email("novoEmail@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_NOT_FOUND);

	}
	
	public void alterarSimulacaoComValorMenor1000() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		String idExtraido = jsonPath.getString("id");
		
		given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("999.00")).parcelas(5).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_NOT_FOUND);

	}
	
	public void alterarSimulacaoComValorIgual1000() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("1000.00")).parcelas(5).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_OK);
	}
	
	public void alterarSimulacaoComValorIgual40000() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("40000.00")).parcelas(5).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_OK);
	}
	
	public void alterarSimulacaoComValorMaiorQue1000() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("1200.00")).parcelas(5).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_OK);
	}	
	
	public void alterarSimulacaoComValorMaiorQue40000() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		String idExtraido = jsonPath.getString("id");
		
		given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("41000.00")).parcelas(5).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_NOT_FOUND);

	}
	

	public void alterarSimulacaoComPacelaMenorQue2() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		String idExtraido = jsonPath.getString("id");
		
		given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("1600.00")).parcelas(1).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_NOT_FOUND);

	}
	
	public void alterarSimulacaoComPacelaMaiorQue48() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		String idExtraido = jsonPath.getString("id");
		
		given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("1600.00")).parcelas(49).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_NOT_FOUND);

	}
	
	public void alterarSimulacaoComPacelaIguala48() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		String idExtraido = jsonPath.getString("id");
		
		given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("1600.00")).parcelas(48).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_OK);

	}
	
	public void alterarSimulacaoComPacelaIguala2() {
		Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir()).email("cristina.silva@email.com")
				.valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

		ResponseBodyExtractionOptions simulacaoCriada =	given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then().extract().body();

		JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
		String cpfExtraido = jsonPath.getString("cpf");
		String idExtraido = jsonPath.getString("id");
		
		given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
				.statusCode(HttpStatus.SC_NO_CONTENT);
		
		Simulacao simulacaoAtualizada = Simulacao.builder().nome("Usuario atualizado").cpf(geradorDeDados.geraCpfSemRepetir()).email("novoEmail@email.com")
				.valor(new BigDecimal("1600.00")).parcelas(2).seguro(true).build();
		
		given().contentType("application/json").pathParam("cpf", cpfExtraido).body(simulacaoAtualizada).when().put("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_OK);

	}
}

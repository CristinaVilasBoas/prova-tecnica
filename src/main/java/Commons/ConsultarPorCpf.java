package Commons;

import static io.restassured.RestAssured.given;

import java.math.BigDecimal;

import org.apache.http.HttpStatus;
import Base.BaseSimulacao;
import Model.Simulacao;
import Util.GeradorDeDados;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.response.ValidatableResponse;

public class ConsultarPorCpf extends BaseSimulacao{
	 GeradorDeDados geradorDeDados = new GeradorDeDados();

	 public ConsultarPorCpf() {
		 BaseSimulacao.initConfig();
	    }

	  public void consultaPorCpfInexistente(String cpf) {
		  Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir())
					.email("cristina.silva@email.com").valor(new BigDecimal("30000.00")).parcelas(5).seguro(true).build();

			ResponseBodyExtractionOptions simulacaoCriada = given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then()
					.extract().body();
			
			JsonPath jsonPath = new JsonPath(simulacaoCriada.asString());
			Integer idExtraido = jsonPath.getInt("id");
			
			given().pathParam("id", idExtraido).when().delete("/v1/simulacoes/{id}").then()
					.statusCode(HttpStatus.SC_NO_CONTENT);
			given().pathParam("cpf", cpf).when().get("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_NOT_FOUND);
		}

		public ValidatableResponse consultarPorCpfExistente() {
			Simulacao simulacao = Simulacao.builder().nome("Cristina").cpf(geradorDeDados.geraCpfSemRepetir())
					.email("cristina.silva@email.com").valor(new BigDecimal("30000.00")).parcelas(5).seguro(true)
					.build();

			given().contentType(ContentType.JSON).body(simulacao).when().post("/v1/simulacoes").then()
					.statusCode(HttpStatus.SC_CREATED);
			return given().pathParam("cpf", simulacao.getCpf()).when().get("/v1/simulacoes/{cpf}").then()
					.statusCode(HttpStatus.SC_OK);
		}
	}

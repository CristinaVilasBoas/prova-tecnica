package Commons;

import io.restassured.response.ValidatableResponse;
import static io.restassured.RestAssured.given;
import org.apache.http.HttpStatus;
import Base.BaseSimulacao;

public class RestricaoCliente extends BaseSimulacao{
	
	 public RestricaoCliente() {
	        BaseSimulacao.initConfig();
	    }

	    public ValidatableResponse retornaCPFComRestricao(String cpf) {
	        return
	            given().
	                pathParam("cpf", cpf).
	            when().
	                get("/v1/restricoes/{cpf}").
	            then().
	                statusCode(HttpStatus.SC_OK);
	    }


	    public void consultaCPFSemRestricao(String cpf) {
	        given().
	            pathParam("cpf", cpf).
	        when().
	            get("/v1/restricoes/{cpf}").
	        then().
	            statusCode(HttpStatus.SC_NOT_FOUND);
	    }
}

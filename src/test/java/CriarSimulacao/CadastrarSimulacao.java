package CriarSimulacao;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import java.math.BigDecimal;
import org.apache.http.HttpStatus;
import org.junit.Test;
import Base.BaseSimulacao;
import Commons.CriaSimulacao;
import Model.Simulacao;
import Util.GeradorDeDados;

public class CadastrarSimulacao {

	GeradorDeDados geradorDeDados = new GeradorDeDados();
	CriaSimulacao criaSimulacao = new CriaSimulacao();
	
	public CadastrarSimulacao() {
		BaseSimulacao.initConfig();
	}

	@Test
	public void verificarRetornoDaSimulacaoPeloCPF() {
		Simulacao simulacao = given().pathParam("cpf", "03452553051").when().get("/v1/simulacoes/{cpf}").then().statusCode(HttpStatus.SC_OK)
				.extract().body().as(Simulacao.class);
		assertThat(simulacao.getNome(), is("Cristina"));
		assertThat(simulacao.getEmail(), is("cristina.silva@email.com"));
		assertThat(simulacao.getValor(), is(new BigDecimal("30000.00")));
		assertThat(simulacao.getParcelas(), is(5));
		assertThat(simulacao.getSeguro(), is(true));
	}

	@Test
	public void criarNovaSimulacaoComSucesso() {
		criaSimulacao.cadastrarSimulacaoComSucesso();
	}
	
	@Test
	public void criarSimulacaoComCpfFormatoInvalido() {
		criaSimulacao.cadastrarSimulacaoComCpfInvalido();
	}
	
	@Test 
	public void criarSimulacaoComEmailEmFormatoInvalido(){
		criaSimulacao.cadastrarSimulacaoComEmailInvalido();
	}
	
	@Test 
	public void criarSimulacaoComValorMenorDe1000() {
		criaSimulacao.cadastrarSimulacaoComValorMenorDe1000();
	}
	
	@Test 
	public void criarSimulacaoComValorMaiorDe41000() {
		criaSimulacao.cadastrarSimulacaoComValorMaiorDe41000();
	}
	
	@Test 
	public void criarSimulacaoComValorMaiorDe1000() {
		criaSimulacao.cadastrarSimulacaoComValorMaiorDe1000();
	}
	
	@Test 
	public void criarSimulacaoComValorMenorDe40000() {
		criaSimulacao.cadastraSimulacaoComValorMenorDe40000();
	}
	
	@Test 
	public void criarSimulacaoComValorIgualDe1000() {
		criaSimulacao.cadastrarSimulacaoComValorIgualDe1000();
	}
	
	@Test 
	public void criarSimulacaoComValorIgualDe40000() {
		criaSimulacao.cadastrarSimulacaoComValorIgualDe40000();
	}
	
	@Test
	public void criarSimulacaoComParcelaIgualA2() {
		criaSimulacao.cadastrarSimulacaoComParcelaIgualA2();
	}
	
	@Test
	public void criarSimulacaoComParcelaMenorA2() {
		criaSimulacao.cadastrarSimulacaoComParcelaMenorA2();
	}
	
	@Test
	public void criarSimulacaoComParcelaMaiorA2() {
		criaSimulacao.cadastrarSimulacaoComParcelaMaiorA2();
	}
	
	@Test
	public void criarSimulacaoComParcelaIgualA48() {
		criaSimulacao.cadastrarSimulacaoComParcelaIgual48();
	}
	
	@Test
	public void criarSimulacaoComParcelaMaiorA48() {
		criaSimulacao.cadastrarSimulacaoComParcelaMaior48();
	}
	
	@Test
	public void criarSimulacaoSemOnome() {
		criaSimulacao.cadastrarSimulacaoSemOnome();
	}
	
	@Test
	public void criarSimulacaoSemOcpf() {
		criaSimulacao.cadastrarSimulacaoSemOcpf();
	}
	
	@Test
	public void criarSimulacaoSemOemail() {
		criaSimulacao.cadastrarSimulacaoSemOemail();
	}
	
	@Test
	public void criarSimulacaoSemOvalor() {
		criaSimulacao.cadastrarSimulacaoSemOvalor();
	}
	
	@Test
	public void criarSimulacaoSemAparcela() {
		criaSimulacao.cadastrarSimulacaoSemOParcela();
	}
	
	@Test
	public void criarSimulacaoSemOseguro() {
		criaSimulacao.cadastrarSimulacaoSemOSeguro();
	}
	
}
package ConsultarTodasSimulacoesCadastradas;

import org.junit.Test;
import Commons.ConsultarSimulacoes;
import Commons.CriaSimulacao;
import Util.GeradorDeDados;

public class ConsultaSimulacoes {
	GeradorDeDados geradorDeDados = new GeradorDeDados();
	ConsultarSimulacoes consultarSimulacoes = new ConsultarSimulacoes();
	CriaSimulacao criaSimulacao = new CriaSimulacao();

	@Test
	public void retornarListaDeSimulacoesCadastradas() {
		criaSimulacao.cadastrarSimulacaoComSucesso();
		consultarSimulacoes.consultarSimulacoes();
	}
}

package RemoverSimulacao;

import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;
import Base.BaseSimulacao;
import Commons.DeletarSimulacao;
import io.restassured.response.ValidatableResponse;

public class RemoverSimulacao {
	DeletarSimulacao deletarSimulacao = new DeletarSimulacao();
	
	public RemoverSimulacao() {
		BaseSimulacao.initConfig();
	}
	
	@Test
	public void excluirSimulacaoComSucesso() {
		deletarSimulacao.deletarSimulacaoCadastrada();
	}
	
	@Test
	public void excluirSimulacaoInexistente(){
		 ValidatableResponse response = deletarSimulacao.deletarSimulacaoNaoCadastrada();
	     response.body("mensagem", is("Simulação não encontrada"));
	}
}

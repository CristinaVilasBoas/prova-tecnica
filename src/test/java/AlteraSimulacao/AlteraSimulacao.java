package AlteraSimulacao;

import org.junit.Test;
import Commons.AlterarSimulacao;

public class AlteraSimulacao {
	
	AlterarSimulacao alteraSimulacao = new AlterarSimulacao();
	
	@Test
	public void alterarSimulacaoComCpfValido(){
		alteraSimulacao.alterarSimulacaoComSucesso();
	}
	
	@Test
	public void alterarSimulacaoComCpfInvalido(){
		alteraSimulacao.alterarSimulacaoInvalida();
	}
	
	@Test
	public void alterarSimulacaoComValorMenor1000() {
		alteraSimulacao.alterarSimulacaoComValorMenor1000();
	}
	
	@Test
	public void alterarSimulacaoComPacelaMenorQue2() {
		alteraSimulacao.alterarSimulacaoComPacelaMenorQue2();
	}
	
	@Test
	public void alterarSimulacaoComEemailInvalido(){
		alteraSimulacao.alterarSimulacaoComEmailInvalido();
	}
	
	@Test
	public void alterarSimulacaoComECpfInvalido(){
		alteraSimulacao.alterarSimulacaoComCpfInvalido();
	}
	
	@Test
	public void alterarSimulacaoComValorMaiorQue40000() {
		alteraSimulacao.alterarSimulacaoComValorMaiorQue40000();
	}
	
	@Test
	public void alterarSimulacaoComPacelaMaiorQue48() {
		alteraSimulacao.alterarSimulacaoComPacelaMaiorQue48();
	}
	
	@Test
	public void alterarSimulacaoComPacelaIguala2() {
		alteraSimulacao.alterarSimulacaoComPacelaIguala2();
	}
	
	@Test
	public void alterarSimulacaoComPacelaIguala48() {
		alteraSimulacao.alterarSimulacaoComPacelaIguala48();
	}
	
	@Test
	public void alterarSimulacaoComValorIgual1000() {
		alteraSimulacao.alterarSimulacaoComValorIgual1000();
	}
	
	@Test
	public void alterarSimulacaoComValorIgual40000() {
		alteraSimulacao.alterarSimulacaoComValorIgual40000();
	}
	
	@Test
	public void alterarSimulacaoComValorMaiorQue1000() {
		alteraSimulacao.alterarSimulacaoComValorMaiorQue1000();
	}
}

package Model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Simulacao {
	    private Long id;
	    private String nome;
	    private String cpf;
	    private String email;
	    private BigDecimal valor;
	    private Integer parcelas;
	    private Boolean seguro;
}

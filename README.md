# Prova sicredi
- Testes de api com a utilização do Rest Assured. 

# Softwares necessários
- Java 8+ JDK deve estar instalado 
- Maven deve estar instalado e configurado no path da aplicação 

# Execução da aplicação: 
- Navegue até a pasta do projeto pelo Terminal através do Prompt de Comando
- Executar o comando mvn spring-boot:run

# Execução dos testes:
- Comando mvn test para execução dos testes

# GET
**Cpf com restrição:** <br />
Descrição: valida get com cpf inválido.<br />
**DADO** que o usuário tenha informado um cpf com restrição, como: 58063164083<br />
**QUANDO** quando for feito um GET<br />
**ENTÃO** o sistema deverá retornar o status 200 e retornar a mensagem: O 58063164083 possui restrição.<br />

**Cpf sem restrição:**<br />
Descrição: valida get com cpf sem restrição.<br />
**DADO** que o usuário tenha informado um cpf que não possua restrição.<br />
**QUANDO** quando for feito um GET<br />
**ENTÃO** o sistema deverá retornar o status 204.<br />

# POST:

Descrição: valida cadastrar com sucesso <br />
**DADO** que o usuário tenha informado os dados para criação de simulação. <br />
**QUANDO** quando for feito um POST <br />
**ENTÃO** o sistema deverá retornar o status 200 e retornar a simulação cadastrada.<br />

Descrição: valida post com nome inválido.<br />
**DADO** que o usuário tenha informado um nome que não seja uma string para criação de uma simulação<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição: valida post com cpf em formato inválido<br />
**DADO** que o usuário tenha informado um cpf que não esteja no formato xxx.xxx.xxx-xx<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá rettornar o status 400.<br />

Descrição: valida post com email em formato inválido<br />
**DADO** que o usuário tenha informado um email que não esteja no formato email@email.com<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição: valida post com valor inválido<br />
**DADO** que o usuário tenha informado um valor inválido<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição: valida post com valor de parcela inválido<br />
**DADO** que o usuário tenha informado uma valor de parcela inválido<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição: valida post com valor inválido para o seguro<br />
**DADO** que o usuário tenha informado um valor inválido para o seguro<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição : valida cadastrar com cpfs iguais<br />
**DADO** que o usuário tenha informado um cpf que já tenha sido cadastrado.<br />
**QUANDO** quando for feito um post<br />
**ENTÃO** o sistema deverá retornar o status 409 e a mensagem : "CPF duplicado".<br />

**Valida obrigatoriedade dos campo:**

Descrição: valida post sem inserir nome.<br />
**DADO** que usuário não preencha o campo de nome para criar uma simulação.<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição: valida post sem inserir o cpf.<br />
**DADO** que o usuário não preencha o campo de cpf para criar uma simulação<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição: valida post sem inserir o email<br />
**DADO** que o usuário não preencha o campo de email para criar uma simulação<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição: valida post sem inserir valor<br />
**DADO** que o usuário não preencha o campo de valor para criar uma simulação.<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição: valida post sem inserir parcela<br />
**DADO** que o usuário não preencha o campo de parcela para criar uma simulação.<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

Descrição: valida post sem inserir seguro<br />
**DADO** que o usuário não preencha o campo de seguro para criar uma simulação<br />
**QUANDO** quando for feito um POST<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />

# GET
Descrição: valida listar todas as simualações<br />
**DADO** que tenha sido cadastrada alguma simulação<br />
**QUANDO**  for feito um get<br />
**ENTÃO** o sistema deverá retornar todas as simulações cadastradas.<br />

Descrição: valida lista vazia<br />
**DADO** que não tenha sido cadastrada nenhum simulação<br />
**QUANDO** for feito um get<br />
**ENTÃO** o sistema deverá retornar o status 204.<br />

Descrição: valida consulta com cpf que foi cadastrado.<br />
**DADO** que tenha sido cadastrada alguma simulação<br />
**QUANDO** for feita uma busca utilizando o cpf como paramêtro<br />
**ENTÃO** o sistema deverá retornar a simulação cadastrada.<br />

Descrição: valida consulta por cpf que não foi cadastrado.<br />
**DADO** que não tenha sido criada uma simulação com um cpf<br />
**QUANDO** for feita uma busca utilizando esse cpf como paramêtro<br />
**ENTÃO** o sistema deverá retornar o status 404.<br />

# PUT
Descrição: valida atualizar com sucesso<br />
**DADO** que tenha sido criada uma simulação<br />
**QUANDO** for feita uma atualização recebendo um cpf como parâmetro<br />
**ENTÃO** o sistema deverá retornar atualizar a simulação.<br />

Descrição: valida atualizar uma simulação com cpf não encontrado<br />
**DADO** que tenha sido criada uma simulação<br />
**QUANDO** for feita uma atualização recebendo como parâmetro um cpf inexistente<br />
**ENTÃO** o sistema deverá retornar o status 404 e a  mensagem : "CPF <cpfExtraido> não encontrado".<br />

Descrição: valida alteração em uma simulação colocando um valor inválido no <atributo>:**<br />
**DADO** que tenha sido criada uma simulação<br />
**QUANDO** for feita uma atualização informando um número inválido no campo de parcelas<br />
**ENTÃO** o sistema deverá exibir <status>.<br />
Atributo    Status <br />
Nome        400    <br />
Cpf         400    <br />
Email       400	   <br />
Valor       400	   <br />
Parcela     400	   <br />
Seguro      400    <br />

Descrição: valida alteração em uma simulação deixando <atributo> vazio:<br />
**DADO** que tenha sido criada uma simulação<br />
**QUANDO** for feita uma atualização deixando o valor vazio<br />
**ENTÃO** o sistema deverá retornar o status 400.<br />
Atributo    Status <br />
Nome        400    <br />
Cpf         400    <br />
Email       400	   <br />
Valor       400	   <br />
Parcela     400	   <br />
Seguro      400    <br />

# DELETE:
Descrição: valida Delete com sucesso<br />
**DADO** que tenha sido criada uma simulação<br />
**QUANDO** for feita a remoção de uma simulação que possui o id cadastrado no sistema<br />
**ENTÃO** o sistema deverá retornar o status 204.<br />

Descrição: valida Delete com id inexistente<br />
**DADO** que tenha sido criada uma simulação<br />
**QUANDO** for feita a remoção de uma simulação com um id inexistente<br />
**ENTÃO** o sistema deverá retornar o status 404.<br />


# Inconsistências detectadas

**Problema 1 -**<br />
Descrição do problema 1: Ao remover uma simulação passando como paramêtro um id válido o status retornado é 200.<br />
Passo a passo para reproduzir o problema:<br />
1- Fazer um delete informando um id que tenha sido cadastrado.<br />
2- O sistema retorna o status 200, porém deveria retornar o status 204.<br />

**Problema 2:**<br />
Descrição do problema 2: Ao remover uma simulação passando como paramêtro um id inválido o status retornado é 200.<br />
Passo a passo para reproduzir o problema 2:<br />
1- Fazer um delete informando um id não esteja na .<br />
2- O sistema retorna o status 200, porém deveria retornar o status 404.<br />

**Problema 3:**<br />
Descrição do problema 3: Ao cadastrar uma simulação com cpf já existente o sistema retorna o status 400. <br />
Passo a passo para reproduzir o problema 7:<br />
1- Fazer um post com o cpf que já foi cadastrado.<br />
2- O sistema retorna o status 400, porém deveria retornar o status 409 pois esse cpf já utilizado.<br />

**Problema 4:**<br />
Descrição do problema 4: Ao fazer um GET de um cpf com restrição a mensagem exibida é: "O cpf <valorCpf> tem problema"<br />
1- Fazer um get passando como paramêtro um cpf que possua restrição.<br />
2- O sistema retorna o status 200 e exibe a mensagem de "O cpf <valorCpf> tem problema", porém a mensagem que deveria ser exibida é: "O cpf <valorCpf> possui restrição".<br />

**Problema 5:**<br />
Descrição do problema 5: Ao fazer um POST passando um valor menor que 1000,00 o status retornado é 201"<br />
1- Fazer um post com o valor menor que 1000,00.<br />
2- O sistema retorna o status 201, porém deveria retornar o status 400.<br />


**Problema 6:**<br />
Descrição do problema 6: Ao fazer um POST passando uma parcela maior que 48 o status retornado é 201"<br />
1- Fazer um post com uma parcela maior que 48.<br />
2- O sistema retorna o status 201, porém deveria retornar o status 400.<br />

**Problema 7:**<br />
Descrição do problema 7: Ao fazer um POST passando uma parcela igual a 2 o status retornado é 201"<br />
1- Fazer um post com uma parcela igual a 2.<br />
2- O sistema retorna o status 201, porém deveria retornar o status 400.<br />


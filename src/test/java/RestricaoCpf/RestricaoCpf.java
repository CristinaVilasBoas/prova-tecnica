package RestricaoCpf;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import Commons.RestricaoCliente;
import io.restassured.response.ValidatableResponse;

public class RestricaoCpf {

   RestricaoCliente restricoesClient =  new RestricaoCliente();
   
    @Test
    public void pessoaComRestricao97093236014(){
        ValidatableResponse response = restricoesClient.retornaCPFComRestricao("97093236014");
        response.body("mensagem", is("O CPF 97093236014 possui restrição"));
    }

    @Test
    public void pessoaComRestricao60094146012() {
        ValidatableResponse response = restricoesClient.retornaCPFComRestricao("60094146012");
        response.body("mensagem", is("O CPF 60094146012 possui restrição"));
    }
    
    @Test
    public void pessoaComRestricao84809766080() {
        ValidatableResponse response = restricoesClient.retornaCPFComRestricao("84809766080");
        response.body("mensagem", is("O CPF 84809766080 possui restrição"));
    }
    
    @Test
    public void pessoaComRestricao62648716050() {
        ValidatableResponse response = restricoesClient.retornaCPFComRestricao("626487160500");
        response.body("mensagem", is("O CPF 626487160500 possui restrição"));
    }
    
    @Test
    public void pessoaComRestricao01317496094() {
        ValidatableResponse response = restricoesClient.retornaCPFComRestricao("01317496094");
        response.body("mensagem", is("O CPF 01317496094 possui restrição"));
    }
    
    @Test
    public void pessoaComRestricao55856777050() {
        ValidatableResponse response = restricoesClient.retornaCPFComRestricao("55856777050");
        response.body("mensagem", is("O CPF 55856777050 possui restrição"));
    }
    
    @Test
    public void pessoaComRestricao19626829001() {
        ValidatableResponse response = restricoesClient.retornaCPFComRestricao("19626829001");
        response.body("mensagem", is("O CPF 19626829001 possui restrição"));
    }
    
    @Test
    public void pessoaComRestricao24094592008() {
        ValidatableResponse response = restricoesClient.retornaCPFComRestricao("24094592008");
        response.body("mensagem", is("O CPF 24094592008 possui restrição"));
    }
    
    @Test
    public void pessoaComRestricao58063164083() {
        ValidatableResponse response = restricoesClient.retornaCPFComRestricao("58063164083");
        response.body("mensagem", is("O CPF 58063164083 possui restrição"));
    }
    @Test
    public void pessoaSemRestricao() {
        restricoesClient.consultaCPFSemRestricao("85893827200");
    }
}

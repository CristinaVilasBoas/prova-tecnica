package Util;

import java.util.ArrayList;
import java.util.Random;

public class GeradorDeDados {
	Random random = new Random();
	
	public String geraCpfSemRepetir() {

		Random gerador = new Random();
		String primeiroNumero = "1";
		StringBuilder strBuilder = new StringBuilder(primeiroNumero);

		for (int i = 0; i < 10; i++) {
			strBuilder.append(String.valueOf(gerador.nextInt(9)));
		}
		String cpf = "88888888888";

		String bloco1 = strBuilder.toString().substring(0, 3);
		String bloco2 = strBuilder.toString().substring(3, 6);
		String bloco3 = strBuilder.toString().substring(6, 9);
		String bloco4 = strBuilder.toString().substring(9, 11);
		return bloco1 + "." + bloco2 + "." + bloco3 + "-" + bloco4;
	}
	
	public String geraNomeAleatorio(){
		ArrayList<String> listaNomes = new ArrayList<String>();
		listaNomes.add("Cristina");
		listaNomes.add("Eliza");
		listaNomes.add("Gabriela");
		listaNomes.add("Raphael");
		listaNomes.add("Rodrigo");
		listaNomes.add("Felipe");
		listaNomes.add("Renato");
		listaNomes.add("Paulo");
		listaNomes.add("Samuel");
		listaNomes.add("Victor");
		return listaNomes.get(random.nextInt(listaNomes.size()));
	}
	
	public String geraEmailValido() {
		ArrayList<String> listaEmail = new ArrayList<String>();
		listaEmail.add("email1@email1.com");
		listaEmail.add("email2@email2.com");
		listaEmail.add("email3@email3.com");
		listaEmail.add("email4@email4.com");
		listaEmail.add("email5@email5.com");
		listaEmail.add("email6@email6.com");
		listaEmail.add("email7@email7.com");
		listaEmail.add("email8@email8.com");
		listaEmail.add("email9@email9.com");
		listaEmail.add("email10@email10.com");
		return listaEmail.get(random.nextInt(listaEmail.size()));
	}
	
}
